import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'sensors-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Output() toggleSidenav = new EventEmitter();

  toggleNav() {
    this.toggleSidenav.emit();
  }
}
