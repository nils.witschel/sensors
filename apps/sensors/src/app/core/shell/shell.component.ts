import { Component, Input, ViewChild } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'sensors-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent {

  @ViewChild('sideNav', {read: MatSidenav}) sideNav: MatSidenav;
  @Input() menu: any[] = [];
  @Input() environment: string;

  navMode = 'side';

  constructor(private mediaObserver: MediaObserver) {
    this.mediaObserver.media$.subscribe(change => {
      switch (change.mqAlias) {
        case 'xs':
        case 'sm':
          this.navMode = 'over';
          this.sideNav.close();
          break;
        default:
          this.navMode = 'side';
          this.sideNav.open();
      }
    });
  }

  toggleSide() {
    this.sideNav.toggle();
  }
}
